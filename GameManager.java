public class GameManager{
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	public GameManager(){
		this.drawPile = new Deck();
		drawPile.shuffle();
		this.centerCard = drawPile.drawTopCard();
		this.playerCard = drawPile.drawTopCard();
	}
	public String toString(){
		return "-------------------------------------------------------------" + "\n" + 
		"Center Card: " + centerCard + "\n" + "Player Card: " + playerCard + "\n" +
		"--------------------------------------------------------------";
	}
	public void dealCards(){
		drawPile.shuffle();
		centerCard = drawPile.drawTopCard();
		playerCard = drawPile.drawTopCard();
		
	}
	public int getNumberOfCards(){
		return drawPile.length();
	}
	public int calculatePoints(){
		if(centerCard.getSuit() == playerCard.getSuit()){
			 return 2;
		}
		else if(centerCard.getValue() == playerCard.getValue()){
			return 4;
		}
		else{
			return -1;
		}
	}
	
	
}